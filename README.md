## Atelier Git

Atelier `Git` du [Louvain-li-Nux](http://louvainlinux.org).

Plus d'informations sur <https://louvainlinux.org/activites/atelier-git>

## Compiler les slides

À la racine du repository, exécutez: `make`, les nouveaux fichiers `theory.pdf` et `exercies.pdf` devraient s'ouvrir à la fin de la compilation, et être accessible dans le dossier `./build_latex`.

Si vous voulez recompiler, il peut être nécessaire d'exécuter `make clean` avant.

Si vous voulez supprimer les fichiers auxiliaires et logs, vous pouvez exécuter `make clean-aux`

Si vous voulez compiler sans ouvrir le pdf, exécutez `make theory` ou `make exercises`, et `make open` si vous voulez juste ouvrir un pdf déjà compilé.

Les options par défaut passées à `latexmk` sont `-pdf -lualatex -cd -silent`, vous pouvez en ajouter en définissant `LATEX_OPT` dans votre environnement ou en passant sa valeur à `make`, par exemple: `make LATEX_OPT=-verbose`.

Vous pouvez aussi ajouter des instructions `bash` à la fin de la commande, en définissant `BASH_POSTPROCESSING` dans votre environnement ou en passant sa valeur à `make`. Les variables spéciales sont accessibles, par exemple pour rediriger l'output complète vers un logfile pour chaque fichier compilé: `make LATEX_OPT=-verbose BASH_POSTPROCESSING=2>&1 1>$(@D)/out.log`, ou encore pour supprimer tous les logs de `latexmk`: `make BASH_POSTPROCESSING=2>/dev/null 1>/dev/null`. Ces instructions sont simplement inséré à la fin de la commande de compilation `latexmk`.

## Corriger les fautes de grammaire/d'orthographe

Nous utiliserons [TeXtidote](https://github.com/sylvainhalle/textidote) lors de la correction du/des fichier(s) *.tex*.

À la racine du repository, exécutez: `make check` si vous avez téléchargé et installé [TeXtidote en version *.deb*](https://github.com/sylvainhalle/textidote/releases/latest/).

Sinon, exécutez: `make check_jar` si vous avez téléchargé [l'archive Java (*.jar*) TeXtidote](https://github.com/sylvainhalle/textidote/releases/latest/download/textidote.jar), déplacez-la dans le dossier `./check_spelling`.

Après l'exécution, un fichier *.html* sera créé dans `./check_spelling` et ouvrez-le avec un navigateur web.

N'hésitez pas à compléter le fichier `./check_spelling/check_words_whitelist.txt` si vous voulez ajoutez des mots dans le dictionnaire du correcteur.

PS: Ne faite pas attention à l'erreur 118 après l'exécution d'une de ces deux commandes.

## Contribuer

N'hésitez pas à proposer des améliorations à ces slides (via pull requests) !

## Déployer des changements

*Information destinée aux membre du Louvain-li-Nux*

La version qui est sur `master` est celle qui est publiée sur [Wiki.js](https://wiki.louvainlinux.org/fr/training/git).

## License

La présentation est disponible sous license `CC-BY 4.0`.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>
<br />This work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

