%----------------------------------------------------------------------------------------
%	PACKAGES
%----------------------------------------------------------------------------------------

\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{indentfirst}
\usepackage{hyperref}
\usepackage{titlesec}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{datetime2}
\usepackage{float}  % Image here [H], top [t], bottom [b] ou page individuelle [p]
\usepackage{algorithm}
\usepackage{algpseudocode}

%----------------------------------------------------------------------------------------
%	PDF SETTINGS SECTIONS
%----------------------------------------------------------------------------------------
\makeatletter
\newcommand{\yeartoday}{\@dtm@year}
\makeatother
\hypersetup{
	pdfauthor = {L'équipe du Louvain-li-Nux},
    pdftitle = {Atelier Git - Document d'Exercices},
}

%----------------------------------------------------------------------------------------
%	DOCUMENT MARGINS
%----------------------------------------------------------------------------------------

\usepackage{geometry} % Required for adjusting page dimensions and margins

\geometry{
	paper=a4paper, % Paper size, change to letterpaper for US letter size
	top=2.5cm, % Top margin
	bottom=3cm, % Bottom margin
	left=2.5cm, % Left margin
	right=2.5cm, % Right margin
	headheight=14pt, % Header height
	footskip=1.5cm, % Space from the bottom margin to the baseline of the footer
	headsep=1.2cm, % Space from the top margin to the baseline of the header
	%showframe, % Uncomment to show how the type block is set on the page
}

%----------------------------------------------------------------------------------------
%	FORMATS
%----------------------------------------------------------------------------------------
\titleformat*{\section}{\LARGE\bfseries}
\titleformat*{\subsection}{\Large\bfseries}
\titleformat*{\subsubsection}{\large\bfseries}

\begin{document}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here

%----------------------------------------------------------------------------------------
%	CUSTOM CODE COLOR SECTION
%----------------------------------------------------------------------------------------
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{codeorange}{rgb}{0.90,0.30,0.30}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{codeorange},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
\begin{center}
\includegraphics[scale=0.3]{../img/logo/louvainlinux_banner.png}
\linebreak
\HRule \\[0.7cm]
{ \Huge \bfseries Atelier Git }\\[0.4cm]
{ \LARGE \bfseries Document d'Exercices }\\[0.7cm]
{ KAP Louvain-li-Nux }\\[0.4cm]
{ \yeartoday }\\[0.1cm]
\HRule \\[0.7cm]
\end{center}

%----------------------------------------------------------------------------------------
%   DOC
%----------------------------------------------------------------------------------------
\begin{center}
    Les \href{https://gitlab.com/louvainlinux/training/atelier-git/-/raw/master/build_latex/theory.pdf}{slides de présentation} se trouve sur notre \href{https://gitlab.com/louvainlinux/}{Gitlab}.
\end{center}

\part{Avant de commencer\dots}
\section{Installation \& Configuration}
\subsection{Installation de Git}
\begin{itemize}
    \item \textbf{Ubuntu} : \texttt{sudo apt-get install git}
    \item \textbf{OS X} : \url{https://sourceforge.net/projects/git-osx-installer/}
    \item \textbf{Windows} : \url{https://git-for-windows.github.io/} (déjà
        installé à l'UCL)
\end{itemize}
Ensuite, créez un compte sur \href{https://github.com/}{Github}.

\subsection{Configuration d'une clé SSH}
\subsubsection{Création d'une clé SSH}
\begin{itemize}
    \item Ouvrez un terminal
    \item Entrez la commande: \verb|ssh-keygen -t ed25519|
    \item Laissez tout par défaut en appuiant sur \verb|Entrée| à chaque question (voir figure \ref{fig:ssh-keygen-exec}).
\end{itemize}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{../img/ssh/ssh-keygen.png}
    \caption{Procédure pour créer sa propre clé SSH sur un \textit{terminal}}
    \label{fig:ssh-keygen-exec}
\end{figure}

Les clés privées et publiques se trouvent dans:\\
\begin{center}
    $\begin{array}{lcl}
        LINUX\ \&\ MACOS & \Rightarrow & \textbf{\$HOME/.ssh/}\\
        WINDOWS & \Rightarrow & \textbf{\%USERPROFILE\%/.ssh/}
    \end{array}$
\end{center}

\subsubsection{Ajout d'une clé SSH sur Github}
\begin{figure}[h]
    \centering
    \includegraphics[width=.4\textwidth]{../img/github/github-ssh.png}
    \caption{Section "SSH keys" dans Github}
    \label{fig:ssh-github-section}
\end{figure}
\begin{enumerate}
    \item Copiez votre clé SSH \textbf{PUBLIQUE} (fichier \textit{id\_ed25519.pub})
    \item Allez dans \textbf{Settings $\rightarrow$ SSH and GPG keys $\rightarrow$ New SSH key} (voir figure \ref{fig:ssh-github-section})
    \item Choisissez un titre à votre clé SSH (n'impacte pas son utilisation)
    \item Collez votre clé SSH \textbf{PUBLIQUE} dans la section correspondante
    \item Cliquez sur \textbf{Add SSH key}
    \item Confirmez avec votre mot de passe
\end{enumerate}

\subsection{Installation d’une interface graphique pour Git (facultatif)}
L’utilisation d’une interface graphique est \underline{\textit{facultatif} voir \textbf{non-recommandé}} car :
\begin{enumerate}
    \item Il existe déjà des interfaces graphiques fournies avec la suite JetBrains, VSCode, \dots
    \item Il est toujours utile d’apprendre comment fonctionne un outil en ligne de commande (\textit{Ce que tout
informaticien qui se respecte devrait faire au moins une fois dans sa vie}).
\end{enumerate}
Si vous souhaitez quand même en utiliser une, nous utiliserons \href{https://desktop.github.com/}{GitHub Desktop}.
\begin{itemize}
    \item Installez GitHub Desktop, puis démarrez-le.
    \item Liez votre compte Github à Github Desktop.
\end{itemize}

% ---------------------------

\section{Organisation}
\subsection{Répartition en groupes}
Tout d'abord, répartissez-vous en 2 grands groupes:
\begin{itemize}
    \item Le groupe \textbf{[GUI] "Interface Graphique"} (utilisant \textit{Github Desktop}).
    \item Le groupe \textbf{[CLI] "Interface Lignes de Commandes"} (utilisant \textit{le terminal}/\textit{Git Bash})
\end{itemize}
Ensuite, formez des \textbf{sous-groupes de 2 ou 3 personnes}.

% ---------------------------
\newpage
% ---------------------------
\part{Exercices}
\section{Créer un dépôt et le partager}
\subsection{\emph{"Un dépôt pour les guider tous, un dépôt pour collaborer"}}
\textbf{Une personne du sous-groupe:}
\begin{itemize}
    \item Allez sur \href{https://github.com/}{Github}.
    \item Créez un nouveau dépôt distant nommé \textit{blagues}.
    \item Ajoutez les personnes de votre sous-groupe en tant que collaborateur de votre nouveau dépôt distant.\\
\end{itemize}

\textbf{Les autres membres du sous-groupe:}
\begin{itemize}
    \item Acceptez l'invitation de collaboration par mail.\footnote{Si vous arrivez sur une page 404 en cliquant sur le lien reçus par mail, connectez-vous à Github et ré-essayez.}
\end{itemize}

\subsection{\emph{"Cloner pour mieux régner"}}
\textbf{Tout les membres du sous-groupe (GUI):}
\begin{itemize}
    \item Ouvrez Github Desktop.
    \item Cliquez sur "\textit{Clone a repository from the Internet}"
    \item Sélectionnez le dépôt souhaité et clonez-le.\\
\end{itemize}

\textbf{Tous les membres du sous-groupe (CLI):}
\begin{itemize}
    \item Allez sur Github, sur la page principale de votre dépôt, et cliquez sur \textit{le bouton vert nommé "Code"} $\Rightarrow$\ \includegraphics[width=.07\textwidth]{../img/github/github_code_button.png}
    \item Choisissez \textit{l'option "SSH"} et copiez la commande.
    \item Ouvrez votre terminal et déplacez-y vous jusqu'à l'endroit où vous voulez cloner votre dépôt distant en utilisant \\ \texttt{cd <dossier>}
    \item Clonez le dépôt distant en utilisant dans le terminal \\
        \verb|git clone <commande-copiée>|
\end{itemize}

\subsection{\emph{"Monde de blague."}}
\label{start-loop-e1}
\subsubsection{\emph{"Blagues ! Vous avez dis des blagues !"}}
\textbf{Une personne du sous-groupe:}
\begin{enumerate}
    \item Allez dans le dossier où vous avez cloné votre dépôt distant\footnote{Emplacement par défaut sur les PCs UCL:
        Z:$\backslash$GitHub$\backslash$blagues.}
        (grâce à l'explorateur de fichiers)
    \item Créez un fichier avec NotePad++, Atom, VSCode, $\dots$ (pas Word !!!) dans le dépôt cloné sur votre ordinateur.
    \item Remplissez le fichier avec des blagues.
        (\href{https://linuxfr.org/news/blagues-d-informaticiens}{Si vous
        n'avez pas d'idées, cliquez ici})
    \item Sauvez le fichier \textit{.txt} $\rightarrow$ \textbf{NE PAS OUBLIER !!!}
\end{enumerate}

\subsubsection{\emph{"envoi-de-blagues-a-mes-amis.txt"}}
\textbf{La même personne (GUI):}
\begin{enumerate}
    \item Reprenez GitHub Desktop et faites un commit.
    \item Faites un push vers le dépôt GitHub en ligne.\\
\end{enumerate}

\textbf{La même personne (CLI):}
\begin{enumerate}
    \item Reprenez le terminal visant votre dépôt local sur votre ordinateur.
    \item Ajoutez le fichier créé à un commit grâce à \\
        \verb|git add <fichier>|
    \item Initialisez un nouveau commit grâce à \\
        \verb|git commit -m "<nom-commit>"|
    \item Envoyer votre commit vers le dépôt GitHub en ligne grâce à \\
        \verb|git push|
\end{enumerate}

\subsection{\emph{"Blagues, des blagues partout"}}
\label{end-loop-e1}
\textbf{Ensuite, les autres, chacun à leur tour:}
\begin{itemize}
    \item Faites un pull.\\
    (\textbf{Groupe CLI}: Sur le terminal grâce à \verb|git pull|)
    \item Regardez l'historique pour vérifier que les changements sont là.\\
        (Sur Github Desktop, sur Github, ou grâce à \verb|git log|\footnote{Conseil: utilisez la touche "q" pour quitter après avoir rentré la commande} sur le terminal).
    \item Ajoutez des blagues dans le fichier et répétez les étapes \ref{start-loop-e1} et \ref{end-loop-e1}.
\end{itemize}
N'hésitez pas à répéter cela plusieurs fois, pour être sûrs de bien comprendre !

% ------------------------------

\section{Merger deux commits}
\subsection{\emph{"Le merge est Puissance"}}
\textbf{Partie à faire individuellement (chacun sur son ordinateur et en même temps):}
\begin{itemize}
    \item Clonez le dépôt précédemment créé si ce n'est pas déjà fait.
    \item Ajoutez une blague dans le fichier \textit{.txt} et sauvegardez-le.
    \item Ajoutez le fichier à votre dépôt local\\
        (avec \textit{Github Desktop} ou avec la commande \verb|add|).
    \item Faites un commit. (pas de push)
    \item Observez et comparez les historiques de chacun.\\
    (\textbf{Groupe CLI}: Pour voir l'historique des commits, utilisez \verb|git log|).
    \item Suivez les slides suivantes en fonction de votre groupe principal et du nombre personnes dans votre sous-groupe.
\end{itemize}

\subsection{\emph{"Six conflits et un merge !"}}
\label{loop-merge-conflict}
\subsubsection{\emph{"À deux, c'est bien$\dots$}}
\textbf{Section pour les groupes de 2 uniquement:}
\begin{itemize}
    \item Une personne fait un \verb|push|. L'autre personne ne fait rien.
    \item L'autre personne fait le \verb|pull| sur son ordinateur.
        \footnote{\scriptsize\textbf{Groupe GUI}: Cliquez sur "push" si "pull" n'est pas affiché, et cliquez "close" sur le message d'erreur qui s'affiche}
        \footnote{\scriptsize\textbf{Groupe CLI}: Si \texttt{pull} ne fonctionne pas, essayez d'abord de faire un \texttt{push} de vos modifications puis réessayez}
    \item Résolvez le conflit de merge ensemble pour avoir les deux blagues (il faut éditer le fichier en question).
    \item Une fois le merge terminé, faites un \verb|commit|, puis un \verb|push|.
    \item L'autre personne peut faire un \verb|pull| pour récupérer la dernière blague.
    \item Comparez à nouveau vos historiques.
\end{itemize}

\subsubsection{\emph{$\dots$mais à trois, c'est mieux"}}
\textbf{Section pour les groupes de 3 uniquement:}
\begin{itemize}
    \item Une personne fait un \verb|push|. Les autres ne font rien.
    \item Une des 2 autres personnes fait le \verb|pull| sur son ordinateur.
        \footnotemark[4] \footnotemark[5]
    \item Résolvez le conflit de merge ensemble pour avoir les deux blagues (il faut éditer le fichier).
    \item Une fois le merge terminé, faites un \verb|commit| et un \verb|push|.
    \item La dernière personne fait le \verb|pull| sur son ordinateur.
    \item Résolvez le conflit de merge ensemble pour avoir toutes les blagues.
    \item Les autres peuvent faire un \verb|pull| pour récupérer toutes les blagues.
    \item Comparez à nouveau vos historiques.
\end{itemize}

\section{Le défaut des fichiers binaires}
\subsection{\emph{"Des blagues qu'on ne peut refuser"}}
\textbf{Une personne du sous-groupe:}
\begin{itemize}
    \item Créez un fichier avec Word (\textit{.docx})/LibreOffice (\textit{.odt}) dans le dépôt \textit{"blagues"} précédemment créé sur votre ordinateur.
    \item Remplissez le fichier avec vos meilleures blagues.
    \item Sauvegardez le fichier \textit{.docx}/\textit{.odt}.
    \item Ajoutez le fichier à votre dépôt local\\
        (avec \textit{Github Desktop} ou avec la commande \verb|add|).
    \item Faites un \verb|commit|.
    \item Faites un \verb|push| sur le dépôt GitHub en ligne.
\end{itemize}

\subsection{\emph{"Le fichier binaire est un mensonge"}}
\textbf{Partie à faire individuellement (chacun sur son ordinateur et en même temps):}
\begin{itemize}
    \item Faites le \verb|pull| du dépôt sur votre ordinateur.
    \item Ajoutez une blague au fichier \textit{.docx}/\textit{.odt}.
    \item Sauvegardez le fichier \textit{.docx}/\textit{.odt}
    \item Faites un \verb|commit|.
\end{itemize}

\subsection{\emph{"\LaTeX$\dots$ \LaTeX~ne meurt jamais"}}
\begin{itemize}
    \item Faites les étapes du \ref{loop-merge-conflict} en fonction du nombre de personnes dans votre sous-groupe.
    \item Enjoy :)
    \item N'hésitez pas à demander pour savoir ce qu'il s'est passé. \#Vive\LaTeX!\\
\end{itemize}
\begin{center}
    \includegraphics[width=.4\textwidth]{../img/image_exercices/explosion_ascii.png}
\end{center}

\end{document}
