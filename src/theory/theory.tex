\documentclass[hyperref={pdfusetitle}]{beamer}
% Language/Font package
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}

% Base package
\usepackage{graphicx}
\usepackage{color}
\usepackage{listings}
\usepackage{todonotes}
\usepackage{comment}
\usepackage{multicol}
\usepackage{datetime2}
%\usepackage{hyperref} % already imported by beamer

\usetheme[progressbar=frametitle]{metropolis}
\definecolor{links}{HTML}{2A1B81}
\hypersetup{colorlinks,linkcolor=darkgray,urlcolor=links}

% New Command: Show only the year
\makeatletter
\newcommand{\yeartoday}{\@dtm@year}
\makeatother

% Title
\title{Présentation Git}
\subtitle{Un outil de collaboration puissant}
\date{\yeartoday}
\author{L'équipe du Louvain-li-Nux}
\institute{KAP Louvain-li-Nux}
\titlegraphic{\hfill\includegraphics[height=2cm]{../img/logo/louvainlinux.png}}
\hypersetup{pdfinfo={Title={Présentation Git - Un outil de collaboration puissant}}}

\begin{document}

% slides pré-atelier pour les personnes en avance
\begin{frame}
\begin{center}
  Suivez cette présentation sur votre ordinateur :

  \fbox{\url{https://wiki.louvainlinux.org/fr/training/git}}
\end{center}

Préparez-vous à utiliser \texttt{git}:
vous utiliserez le logiciel GitHub Desktop durant cette présentation.

Prenez un peu d'avance, créez-vous un compte Github et installez Github Desktop:
\begin{itemize}
    \item Sur les ordinateurs Windows UCL: installez
            \url{https://desktop.github.com}
    \item Ou installez GitHub Desktop sur votre ordinateur:
        \begin{itemize}\label{install}
        \item \textbf{Ubuntu}: {\footnotesize{\url{https://github.com/shiftkey/desktop/releases}}}
        \item \textbf{Windows ou OS X}: \url{https://desktop.github.com}
    \end{itemize}
\end{itemize}
\end{frame}

\maketitle

\begin{frame}{Cette présentation est\dots}
    \begin{itemize}
        \item Sous licence libre CC-BY 4.0.
        \item En ligne (slides en pdf et sources \LaTeX, exercices\ldots):
            \small{\url{https://wiki.louvainlinux.org/fr/training/git}}
    \end{itemize}
    \begin{center}
        \href{https://gitlab.com/louvainlinux/training/atelier-git/-/raw/master/build_latex/theory.pdf}{\includegraphics[width=0.5\textwidth]{../img/qr-code/theory_link_qr.png}}
    \end{center}
\end{frame}

\begin{frame}{Remerciements}
    Merci à
    Denis \textsc{Pettens},
    Gaëtan \textsc{Cassiers},
    Pablo \textsc{Gonzalez Alvarez},
    Alexandre \textsc{Fiset},
    Nathan \textsc{Cloos},
    Pierre \textsc{Ortegat},
    Martin \textsc{Vandenbussche},
    Laurent \textsc{Ziegler},
    Louis \textsc{Arys},
    Morgane \textsc{Leclerc},
    Théo \textsc{Vanden Driessche}
    et
    Guillaume \textsc{Jadin}
    pour la réalisation des précédentes versions de ces slides.
\end{frame}

\begin{frame}{Table des matières}
%\small
\begin{multicols}{2}
    \setbeamertemplate{section in toc}[sections numbered]
    \tableofcontents[hideothersubsections]
\end{multicols}
\end{frame}

%-------------------------------------------------------------------%

\section{Introduction}
\begin{frame}{Gérer un projet}
Comment gérez-vous actuellement un projet ?

\begin{itemize}
    \item L'envoyer à travers un message sur Facebook,\dots (\textbf{Très mauvaise idée})
    \item L'envoyer par mail (\textbf{Un peu moins})
    \item Utiliser une Dropbox, Google Drive,\dots (\textbf{Déjà mieux, mais toujours risqué ou manque de fonctionnalités})
\end{itemize}

Solution : Utiliser un \textbf{système de gestion de version décentralisé}
(Distributed Version Control System (DVCS) pour les anglophiles).
\end{frame}

\begin{frame}{Un DVCS ?}
    \begin{itemize}
        \item \textbf{Version:} Enregistre des \og{}instantanés\fg{} du projet.
        \item \textbf{Gestion:} Revenir en arrière, voir des différences,
            fusionner des modifications.
        \item \textbf{Décentralisé:} Chacun
            \begin{itemize}
                \item a sa copie (avec son historique) sur son PC,
                \item peut mettre sa copie (et son historique) en ligne,
                \item peut récupérer sur son PC les copies et historiques disponibles en ligne,
                \item peut fusionner différentes copies (semi-)automatiquement.
            \end{itemize}
        \item \textbf{Projet:} n'importe quel répertoire (\og dossier\fg) sur votre ordinateur. Donc
            n'importe quoi: Bureautique, \LaTeX, code, image, musique\dots
    \end{itemize}
\end{frame}

\begin{frame}{Et Git dans tout ça?}
\texttt{Git} a été créé en 2005 par Linus Torvalds (auteur de
\texttt{Linux}); le plus connu et utilisé.

À l'origine, interface en ligne de commande.

Aujourd'hui: aussi des interfaces graphiques, dont GitHub Desktop.
\end{frame}

\begin{frame}{Mais on m'avait parlé de GitHub !}
    Souvenez-vous...
    \begin{itemize}
        \item \textbf{Décentralisé} Chacun
            \begin{itemize}
                \item peut mettre sa copie (et son historique) en ligne,
                \item \dots
            \end{itemize}
    \end{itemize}

    Il y a plein "d'endroits" en ligne où on peut envoyer son travail, GitHub, BitBucket et GitLab sont les plus connus.

    En plus de ça, ceux-ci ont des fonctionnalités pour interagir avec des collaborateurs.
\end{frame}

%-------------------------------------------------------------------%

\section{Principes de Git}
\begin{frame}{Concepts}
    \begin{itemize}
        \item \textbf{Espace de travail:}
            les fichiers, répertoires... dans lesquels on
            travaille. Ils n'ont rien de spécial par rapport à d'autres dossiers
            sur l'ordinateur.
        \item \textbf{Dépôt:} espace de travail + historique, sur un ordinateur.
        \item \textbf{Commit:} "version", est le successeur d'une autre commit.
        \item \textbf{Historique:} la "chaîne" de tous les commits, du plus ancien au plus récent.
        \item \textbf{Dépôt distant:} un dépôt qui se trouve chez GitHub.
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Concept: le \textbf{commit}}

\begin{center}
    \includegraphics[width=0.9\textwidth]{../img/features/commits/commits.png}
\end{center}
\footnotesize{Les illustrations non sourcées viennent de \url{https://git-scm.com/book}.}
\end{frame}

\begin{frame}{Actions}
    \begin{itemize}
        \item \textbf{Créer} un dépôt sur GitHub.
        \item \textbf{Cloner} (faire une copie d') un dépôt de GitHub sur son PC.
        \item \textbf{Modifier/créer} des fichiers (pas avec Git!).
        \item \textbf{Ajouter} un fichier modifié: il sera pris en compte dans le
            prochain commit.
        \item \textbf{Faire un commit}: créer une nouvelle version, qui contient tous
            les fichiers ajoutés. On y ajoute un commentaire (qui décrit les
            changements).
    \end{itemize}
\end{frame}
\begin{frame}{Actions}
    \begin{itemize}
        \item \textbf{Consulter} un historique.
        \item \textbf{Push}: envoyer ses nouveaux commits sur GitHub.
        \item \textbf{Pull}: récupérer des changements (qui ont été envoyés
            par quelqu'un d'autre) depuis GitHub.
        \item \textbf{Merge}: quand on Pull et qu'on a aussi des nouveaux commits sur
            son PC. Git essaye de fusionner automatiquement; s'il ne sait pas
            le faire, il demande à l'utilisateur.
    \end{itemize}
\end{frame}

\begin{frame}[standout]
    Questions ?
\end{frame}

%-------------------------------------------------------------------%

\section{Initiation à Github}
\begin{frame}{Github - Menu principal}
    \includegraphics[width=1\textwidth]{../img/github/github-menu.png}
\end{frame}

\begin{frame}{Github - Créer un nouveau dépôt distant}
    \includegraphics[width=0.8\textwidth]{../img/github/github-create-repo.png}
\end{frame}

\begin{frame}{Github - Vue globale d'un dépôt distant}
    \includegraphics[width=1\textwidth]{../img/github/github-repo.png}
\end{frame}

\begin{frame}{Github - Paramètres d'un dépôt distant}
    \includegraphics[width=1\textwidth]{../img/github/github-repo-settings.png}
\end{frame}

\begin{frame}{Github - Ajouter un collaborateur à un dépôt distant}
    \includegraphics[width=1\textwidth]{../img/github/github-repo-settings-add-collab.png}
\end{frame}

%-------------------------------------------------------------------%

\section{Principes d'une clé SSH}
\begin{frame}{Une clé SSH ? C’est (pour)quoi ?} \label{ssh-key}
    \begin{itemize}
        \item SSH = \textbf{S}ecure \textbf{SH}ell Protocol
        \item Accès \underline{sécurisé} écriture + lecture à un dépôt distant
        \item Identifie un PC avec une clé \textbf{unique}
        \item Composé de \textit{deux} clés :
        \begin{itemize}
            \item Clé publique (À copier et partager vers des services de confiance)
            \item Clé privé (\textsc{À garder secret !!!})
        \end{itemize}
        \item Comparé à un cadenas :
        \begin{itemize}
            \item Clé publique $\rightarrow$ Cadenas qui \textbf{protège} quelque chose
            \item Clé privé $\rightarrow$ Clé \textbf{unique} qui \textbf{ouvre} les cadenas
        \end{itemize}
        \item Pas besoin de mot de passe
    \end{itemize}
\end{frame}

%-------------------------------------------------------------------%

\section{Initiation à Git en ligne de commandes (CLI)}
\subsection{Commandes basiques}
\begin{frame}{Commandes basiques: Initialisation} \label{CLI-init}
    \begin{flushleft}
        Définir son profil: \\
        \texttt{git config --global user.name <nom-complet>} \\
        \texttt{git config --global user.email <email>} \\
        \texttt{git config --global user.editor <text-editor>}

        Créer un dépôt local: \\
        \texttt{git init}

        Ajouter un dépôt distant: \\
        \texttt{git remote add <remote-url>}

        Cloner un dépôt distant en local: \\
        \texttt{git clone <remote-url>}

        Ignorer un fichier: Ajouter le path du fichier dans \texttt{.gitignore}
    \end{flushleft}
\end{frame}

\begin{frame}{Commandes basiques: Vérification} \label{CLI-check}
    \begin{flushleft}
        Voir l'état du repos et les fichiers ajoutés: \\
        \texttt{git status}

        Visualisez l'historique: \\
        \texttt{git log}
    \end{flushleft}
\end{frame}

\begin{frame}{Commandes basiques: Traitement}  \label{CLI-treat}
    \begin{flushleft}
        Ajouter des fichiers: \\
        \texttt{git add <fichiers>} (ou \texttt{git add .} pour ajouter tout les fichiers ajoutés/modifiés)

        Commit les changements ajoutés: \\
        \texttt{git commit}

        Récupérer des commits du dépôt distant: \\
        \texttt{git pull}

        Envoyer des commits au dépôt distant: \\
        \texttt{git push}

        Merge une branche dans la branche actuelle: \\
        \texttt{git merge <branch>}
    \end{flushleft}
\end{frame}

%-------------------------------------------------------------------%

\section{Initiation à Github Desktop (GUI)}
\begin{frame}{Créer un nouveau dépôt local}
    \begin{columns}
        \begin{column}{.5\textwidth}
            \includegraphics[width=\textwidth]{../img/github_desktop/add_repo.png}\\
            {\small File $\rightarrow$ Add Local repository \emph{Ctrl+O}}
        \end{column}
        \begin{column}{.5\textwidth}
            \includegraphics[width=\textwidth]{../img/github_desktop/create_local_repo.png}\\$ $
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Publier un dépot sur Github}
    \includegraphics[width=.9\textwidth]{../img/github_desktop/publish_repo.png}
    \\
    {\small Pour accéder au dépot en ligne: Repository $\rightarrow$ View on Github \emph{Ctrl+Shift+G}}
\end{frame}

\begin{frame}{Ajouter des fichiers}
    \begin{center}
        \includegraphics[width=\textwidth]{../img/github_desktop/see_added.png}
    \end{center}
\end{frame}

\begin{frame}{Remarque: fichier texte vs binaire}
    \begin{itemize}
        \item Fichiers texte: programme, \LaTeX\dots\\
            \begin{center}
                \includegraphics[width=.75\textwidth]{../img/github_desktop/see_added.png}\\
            \end{center}
        \item Fichiers binaires: le reste: Word, Writer, images, sons, PDF\dots\\
            \begin{center}
                \includegraphics[width=.75\textwidth]{../img/github_desktop/binary_file.png}
            \end{center}
    \end{itemize}
\end{frame}

\begin{frame}{Créer un commit}
    \begin{itemize}
        \item Créer un \texttt{commit} sur base des fichiers ajoutés.
        \item Message de \texttt{commit}: décrit les changements effectués.
    \end{itemize}
    \begin{center}
        \includegraphics[width=.5\textwidth]{../img/github_desktop/commit.png}
    \end{center}
\end{frame}

\begin{frame}{Visualiser l'historique}
    \includegraphics[width=.9\textwidth]{../img/github_desktop/historique.png}
\end{frame}

\begin{frame}{Astuce: ignorer des fichiers}
    Des fichiers que vous ne voulez jamais dans Git (résultats de compilation,
    fichiers temporaires\dots)? Cachez-les!

    NB: Cela crée un fichier \texttt{.gitignore}: celui-là, on le versionne.
    \begin{center}
        \includegraphics[width=.5\textwidth]{../img/github_desktop/ignore.png}
    \end{center}
\end{frame}

\begin{frame}{Pull: récupérer des commits qui sont sur GitHub}
    \begin{center}
        \includegraphics[width=\textwidth]{../img/github_desktop/pull_2.png}
    \end{center}
\end{frame}

\begin{frame}{Push: envoyer des commits sur GitHub}
    \begin{center}
        \includegraphics[width=\textwidth]{../img/github_desktop/push_2.png}
    \end{center}
\end{frame}

\begin{frame}{Merge non-automatique: quand il y a des conflits}
    Message d'erreur:
    \begin{center}
        \includegraphics[width=.9\textwidth]{../img/github_desktop/conflit_1.png}
    \end{center}
\end{frame}

\begin{frame}{Merge non-automatique: quand il y a des conflits}
    Trouver le(s) fichier(s) en conflit:
    \begin{center}
        \includegraphics[width=.27\textwidth]{../img/github_desktop/conflit_2.png}
    \end{center}
\end{frame}

\begin{frame}{Merge non-automatique: quand il y a des conflits}
    Trouver le(s) endroit(s) en conflit dans le fichier (reconnaissables par des balises) :

    Avant:
    \begin{center}
        \includegraphics[width=\textwidth]{../img/github_desktop/merge_before.png}
    \end{center}

    Après:
    \begin{center}
        \includegraphics[width=\textwidth]{../img/github_desktop/merge_after.png}
    \end{center}
\end{frame}

\begin{frame}{Merge non-automatique: quand il y a des conflits}
    Choisir la version que l'on veut garder et commit:\\
    \begin{center}
        \includegraphics[width=.3\textwidth]{../img/github_desktop/conflit_4.png}\\
        \includegraphics[width=.95\textwidth]{../img/github_desktop/conflit_5.png}
    \end{center}
\end{frame}

\begin{frame}{Astuce: de l'aide !}
    On peut trouver de l'aide:

    \begin{center}
        Github help:\\ \url{https://help.github.com/}
    \end{center}
    \begin{center}
        Github desktop help:\\ \url{https://help.github.com/desktop/}
    \end{center}
\end{frame}

%-------------------------------------------------------------------%

\section{Exercices}

\begin{frame}{Avant de commencer...}
    Tout d'abord, répartissez-vous en 2 grands groupes:
    \begin{itemize}
        \item Le groupe \textbf{[GUI] "Interface Graphique"} (utilisant \textit{Github Desktop}).
        \item Le groupe \textbf{[CLI] "Interface Lignes de Commandes"} (utilisant \textit{le terminal}/\textit{Git Bash})
    \end{itemize}
    Ensuite, formez des \textbf{sous-groupes de 2 ou 3 personnes}.
\end{frame}

\begin{frame}{Initialisation et Installation}
    \textbf{Pour tout le monde:}
    \begin{itemize}
        \item Créez un compte sur \href{https://github.com}{GitHub}.
        \item Installez l'interface Git [GUI + CLI] (voir liens slide~\ref{install-cli}).
        \item Créez une clé SSH sur votre ordinateur (voir pourquoi slide~\ref{ssh-key}).
        \item Ajoutez la clé SSH créée sur Github.
    \end{itemize}

    \textbf{Pour le groupe GUI:}
    \begin{itemize}
        \item Installez GitHub Desktop, (voir liens slide~\ref{install}), puis ouvrez-le.
        \item Liez votre compte Github à Github Desktop.
    \end{itemize}
\end{frame}

\begin{frame}{Document d'Exercices}
    Téléchargez le document d’exercices sur votre ordinateur :

    \fbox{\url{https://wiki.louvainlinux.org/fr/training/git}}
    \begin{center}
        \href{https://gitlab.com/louvainlinux/training/atelier-git/-/raw/master/build_latex/exercises.pdf}{\includegraphics[width=0.5\textwidth]{../img/qr-code/exercises_link_qr.png}}
    \end{center}
\end{frame}


%-------------------------------------------------------------------%

\section{Concepts plus avancés}
\subsection{Les objets git et leurs parents}
\begin{frame}{Concept: Les objets git}
    \begin{itemize}
        \item Chaque commit a un identifiant: \textbf{12f87}b95caff8cbeb5ce0717528d77e27db5669c.
    \end{itemize}
    \begin{center}
    \includegraphics[width=0.8\textwidth]{../img/features/commits/commit-and-tree.png}
    \end{center}
\end{frame}

\begin{frame}{Concept: Les parents}
    \begin{itemize}
        \item Chaque commit a un parent.
    \end{itemize}
    \includegraphics[width=\textwidth]{../img/features/commits/commits-and-parents.png}
\end{frame}

\subsection{Les étiquettes/tags}
\begin{frame}{Concept: Les étiquettes (ou tags) [1/2]}
    \begin{itemize}
        \item Les étiquettes (ou tags) sont des références pointant vers des commits spécifiques dans l'historique d'un projet Git.
        \item \texttt{HEAD} est un tag pointant la position actuelle. (Par défaut, la position du dernier commit ajouté)
    \end{itemize}
    \includegraphics[width=0.9\textwidth]{../img/features/branch/branch-and-history.png}
\end{frame}

\begin{frame}{Concept: Les étiquettes (ou tags) [2/2]}
    Les tags sont généralement utilisés pour lier un commit spécifique à une version précise du projet (\texttt{v1.0} par exemple).

    Contrairement aux branches, les tags ne changent pas et donc ne stockent pas l'historique des commits après leur création.
\end{frame}

\subsection{Les branches}
\begin{frame}{Concept: Les branches [1/4]}
    \begin{itemize}
        \item Une branche est une étiquette qui change et possède un historique.
        \item La branche par défaut est \texttt{main} (anciennement \texttt{master}).
    \end{itemize}
    \begin{center}
        \includegraphics[width=0.8\textwidth]{../img/features/branch/head-to-main.png}
    \end{center}
\end{frame}

\begin{frame}{Concept: Les branches [2/4]}
    \begin{itemize}
        \item Des branches utilisables durablement ou temporairement peuvent être créées pour une meilleur gestion et séparation du projet.\\
        \item Une branche de développement est \textbf{extrêmement recommandée} pour effectuer des modifications instables sur le projet (tout en gardant l'actuelle version du projet dans la branche \texttt{main}).\\
        Les noms par défaut de cette branches sont: \texttt{dev}, \texttt{devel}, \texttt{develop}, \texttt{test} ou \texttt{testing}.
    \end{itemize}
\end{frame}

\begin{frame}{Concept: Les branches [3/4]}
    La branche courante est celle qui suit les nouveaux commits.
    \begin{columns}
        \begin{column}{0.44\textwidth}
            \begin{center}
                \includegraphics[width=\textwidth]{../img/features/branch/head-to-dev.png}
            \end{center}
        \end{column}
        \begin{column}{0.54\textwidth}
            \includegraphics[width=\textwidth]{../img/features/branch/advance-dev.png}
        \end{column}
    \end{columns}
    Dans ce cas ci, le commit \texttt{87ab2} a été ajouté à la branche \texttt{dev}.\\
    Ainsi, la branche \texttt{dev} contient un nouveau commit alors que la branche \texttt{main} pointe toujours vers le même commit.
\end{frame}

\begin{frame}{Concept: Les branches [4/4]}
    \begin{itemize}
        \item Les branches peuvent partir d'un commit précis et diverger en plein de commits différents
        \item Utilité: Pouvoir travailler sur des modifications indépendantes.
    \end{itemize}
    \begin{center}
        \includegraphics[width=0.9\textwidth,trim=0 0 0 40, clip]{../img/features/merge/advance-main.png}
    \end{center}
\end{frame}

\subsection{La fusion de commits (merging)}
\begin{frame}{Concept: La fusion de commits [1/3]}
    \begin{center}
        \includegraphics[width=0.9\textwidth,trim=0 0 0 40, clip]{../img/features/merge/basic-merging-1.png}
        \includegraphics[width=\textwidth,trim=0 0 0 60, clip]{../img/features/merge/basic-merging-2.png}
    \end{center}
\end{frame}

\begin{frame}{Concept: La fusion de commits [2/3]}
    Dans le cas ci-dessus, le commit \texttt{C5} de la branche \texttt{iss53} a été fusionné avec/dans le commit \texttt{C4} de la branche \texttt{main} pour former un nouveau commit de fusion \texttt{C6} (dans \texttt{main}).

    \textbf{Bonne pratique}: Pour ne pas encombrer vos depôts de branches \underline{temporaires} inutiles, étant donné que la branche \texttt{iss53} n'a plus d'utilité, il est fortement recommandé de la supprimer.
\end{frame}

\begin{frame}{Concept: La fusion de commits [3/3]}
    Dans certains cas, Git arrive à effectuer des fusions de commits automatiquement.

    Cependant, il arrive qu'il ne puisse pas faire ces fusions car minimum deux fichiers ont été modifié aux même endroits dans deux branches différentes.

    Dans ce cas, l'utilisateur doit régler les conflits manuellement.
\end{frame}

%-------------------------------------------------------------------%

\section{Commandes plus avancées (CLI)}
\subsection{Les branches}
\begin{frame}{Commandes avancées: Les branches locales}
    \begin{flushleft}
        Déplacer la \texttt{HEAD} vers une branche: \\
        \texttt{git switch <ref>}

        Afficher les branches locales: \\
        \texttt{git branch}

        Créer localement une nouvelle branche: \\
        \texttt{git branch <new-branch>}

        Supprimer localement une branche: \\
        \texttt{git branch -d <branch>}

        Forcer localement la suppression d'une branche \textbf{(PRUDENCE)}: \\
        \texttt{git branch -D <branch>}

        Renommer localement la branche actuelle en \texttt{<branch>}: \\
        \texttt{git branch -m <branch>}
    \end{flushleft}
\end{frame}

\begin{frame}{Commandes avancées: Les branches distantes}
    \begin{flushleft}
        Afficher les branches locales et distantes:\\
        \texttt{git branch -a}\\
        Afficher les branches distantes:\\
        \texttt{git branch -r}

        Ajouter une branche du dépôt local vers le dépôt distant: \\
        \texttt{git push -u origin <branch>}

        Supprimer une branche du dépôt distant: \\
        \texttt{git push origin -d <branch>}\\
        \texttt{git push origin :<branch>}
    \end{flushleft}
\end{frame}

\subsection{Les étiquettes/tags}
\begin{frame}{Commandes avancées: Les étiquettes (ou tags)}
    \begin{flushleft}
    Déplacer la \texttt{HEAD} vers un commit, un tag ou une branche: \\
    \texttt{git checkout <ref>}

    Créer un tag annotés: \\
    \texttt{git tag -a <tagname> <commit> -m "<msg>"}
\end{flushleft}
\end{frame}

%-------------------------------------------------------------------%

\section*{Autres modèles de collaboration}
\subsection{Fork et Pull Request}
\begin{frame}{Qu'est-ce qu'un Fork ?}
    Une autre méthode de collaboration, très utilisée pour des larges projets
    et/ou projets où la contribution est ouverte à tous est le \textbf{fork} d'un projet.

    Ainsi, un \textbf{fork} d'un projet correspond à la copie d'un dépôt en ligne appartenant à un utilisateur vers un autre utilisateur ayant pour objectif d'améliorer le projet initial.
\end{frame}

\begin{frame}{Fork, Copie ou Clone ?}
    Le fork n'est pas à confondre avec le \texttt{clone} d'un projet:
    \begin{itemize}
        \item Copier un dépôt en ligne sur un PC.
        \item Impossibilité d'ajouter des commits,\dots (si le projet n'appartient pas à l'utilisateur).
    \end{itemize}
    Et avec la \texttt{copie} d'un projet:
    \begin{itemize}
        \item Effectuer des modifications personnelles sans modifier le dépôt originel.
        \item Ajouter des commits, des branches,\dots
    \end{itemize}

    Ainsi, la différence majeure entre un fork et une simple copie (pas un clone donc) d'un dépôt réside dans l'objectif voulu par la personne effectuant cette copie.
\end{frame}

\begin{frame}{Avantages d'un Fork}
    Par rapport à la simple copie, le fork a les avantages suivants:
    \begin{itemize}
        \item L'utilisateur peut effectuer des modifications personnelles sans modifier le dépôt originel dans l'objectif d'améliorer le projet initial.
        \item Le responsable du dépôt initial peut vérifier les modifications apportées au projet en ayant la possibilité d'accepter ou refuser les modifications ainsi que de suggérer des idées.
    \end{itemize}

    Lorsque l'utilisateur veut envoyer ses modifications vers projet initial, il envoit un \texttt{pull request} au responsable du projet qui peut accepter ou refuser la demande de fusion.
\end{frame}

\begin{frame}{Utilisation d'un Fork et Pull Request [1/2]}
    Les schémas ci-dessous démontrent le processus d'utilisation et le fonctionnement du fork et du pull request.
    \begin{center}
        \includegraphics[width=\textwidth]{../img/features/fork/github-setup.png}
    \end{center}
\end{frame}

\begin{frame}{Utilisation d'un Fork et Pull Request [2/2]}
    \begin{small}
        Voir \url{https://help.github.com/articles/fork-a-repo/}.
    \end{small}
    \begin{center}
        \includegraphics[width=0.9\textwidth]{../img/features/fork/github-workflow.jpg}
    \end{center}
\end{frame}

\begin{frame}{Forker un dépôt sur GitHub}
    \includegraphics[scale=0.30]{../img/github/github-fork.png}
\end{frame}

%-------------------------------------------------------------------%

\section{Solutions aux Problèmes [SPs]}
\subsection{Initialiser un dépôt local et le mettre en ligne}
\begin{frame}{[SPs] Initialiser un dépôt local et le mettre en ligne}
    \begin{flushleft}
        \begin{scriptsize}
            NB: \texttt{<remote-url>} $\Rightarrow$ \textit{git@<depot>:<user>/<repo-name>.git}
            \begin{itemize}
                \item \texttt{<depot>} $\Rightarrow$ site où le dépôt sera hébergé,
                \item \texttt{<user>} $\Rightarrow$ votre nom d'utilisateur/groupe sur le site \texttt{<depot>},
                \item \texttt{<repo-name>} $\Rightarrow$ le nom de notre dépôt.
            \end{itemize}
            Exemple: \textit{git@gitlab.com:louvainlinux/exercise-git.git}
        \end{scriptsize}
        \begin{footnotesize}
            \begin{enumerate}
                \item Créer un dossier ayant le nom de votre nouveau dépôt (nommé \texttt{<repo-name>} pour l'exemple).

                \item Déplacer-vous dans \texttt{<repo-name>} et créer un nouveau dépôt local:
                \texttt{git init}

                \item Ajouter un lien vers votre nouveau dépôt local:\\
                \texttt{git remote add origin <remote-url>}

                \item Ajouter un commit pour initialiser le dépôt (par défaut en ajoutant un fichier "README.md"),

                \item Ajouter la branche principale (\texttt{main} par défaut) sur le dépôt distant:\\
                \texttt{git push -u origin <branch>}
            \end{enumerate}
        \end{footnotesize}
    \end{flushleft}
\end{frame}

\subsection{Passer un dépôt cloné de HTTPS à SSH}
\begin{frame}{[SPs] Passer un dépôt cloné de HTTPS à SSH}
    \begin{flushleft}
    \begin{small}
    \begin{enumerate}
        \item Vérifiez si le remote \texttt{origin} est en HTTPS ou en SSH grâce à la commande: \\
        \quad \texttt{git remote -v} \\
        \begin{itemize}
            \item Si HTTPS:\\
            \begin{tiny}
                \texttt{> origin  https://gitlab.com/louvainlinux/atelier-git.git (fetch)}\\
                \texttt{> origin  https://gitlab.com/louvainlinux/atelier-git.git (push)}\\
            \end{tiny}

            \item Si SSH:\\
            \begin{tiny}
                \texttt{> origin  git@gitlab.com:louvainlinux/atelier-git.git (fetch)}\\
                \texttt{> origin  git@gitlab.com:louvainlinux/atelier-git.git (push)}\\
            \end{tiny}
        \end{itemize}

        \item Si vous êtes en HTTPS, utilisez la commande: \\
        \quad \texttt{git remote set-url <remote> <remote-url>}\\
        \quad où \texttt{<remote>} est: \texttt{origin}\\
        \quad et \texttt{<remote-url>} est l'url copié lors du clone d'un dépôt distant en local. Dans notre exemple, c'est: \\
        \texttt{git@gitlab.com:louvainlinux/atelier-git.git}\\

        \item Refaite l'étape n°1 pour voir si le remote \texttt{origin} a changé.
    \end{enumerate}
    \end{small}
    \end{flushleft}
\end{frame}

\subsection{Retirer des fichiers ajoutés qui n'ont pas été commit}
\begin{frame}{[SPs] Retirer des fichiers ajoutés qui n'ont pas été commit}\label{how-to-revert-add}
    \begin{flushleft}
        Utilisez la commande:\\
        \quad \texttt{git reset}\\
        lorsque vous avez utilisé les commandes: \\
        \quad \texttt{git add <files>}\\
        \quad \texttt{git rm <files>}\\
        pour annuler ces dernières.

        \noindent\makebox[\linewidth]{\rule{\paperwidth}{0.4pt}}

        D'une manière purement théorique,\\Cette commande, sans options, va réinitialiser la position actuelle \texttt{HEAD} au dernier commit effectué.
    \end{flushleft}
\end{frame}

\subsection{Retirer les n derniers commits locaux qui n'ont pas encore été push sur un dépôt distant}
\begin{frame}{[SPs] Retirer les \textit{n} derniers commits locaux qui n'ont pas encore été push sur un dépôt distant}
    \begin{flushleft}
    \begin{small}
    \begin{itemize}
        \item Pour supprimer \textit{le dernier commit effectué}, en \textbf{gardant} le travail que vous avez fait jusque ici, utilisez la commande: \\
        \quad \texttt{git reset --soft HEAD\sim1}

        \item Pour supprimer \textit{le dernier commit effectué}, en \textbf{supprimant} le travail que vous avez fait jusque ici, utilisez la commande: \\
        \quad \texttt{git reset --hard HEAD\sim1}

        \item Si vous voulez revenir en arrière de plusieurs commits (non-push), utilisez les deux commandes ci-dessus en modifiant \texttt{HEAD\sim1} en:\\
        \quad \texttt{HEAD\sim2} pour revenir en arrière de 2 commits,\\
        \quad \texttt{test\sim3} pour revenir en arrière de 3 commits sur la branche \texttt{test}, \dots\\

        \item (voir slide~\ref{how-to-revert-add} pour retirer les fichiers ajoutés).
    \end{itemize}
    \end{small}
    \end{flushleft}
\end{frame}

\subsection{Revenir de manière permanente à un commit spécifique d'une branche (sans garder l'historique après ce dernier)}
\begin{frame}{[SPs] Revenir \textsc{de manière permanente} à un commit spécifique d'une branche \textsc{\emph{(sans garder l'historique après ce dernier)}}}
    \begin{flushleft}
    \begin{small}
    \begin{enumerate}
        \item Trouvez l'id du commit auquel vous voulez retourner en utilisant la commande: \\
        \quad \texttt{git log}

        \item Utilisez la commande: \\
        \quad \texttt{git reset --hard <commit-id>}\\
        où \texttt{<commit-id>} est l'id du commit auquel on veut retourner.

        \item Forcez un push sur la branche que vous désirez en utilisant la commande: \\
        \quad \texttt{git push -f origin <branch-name>}

        \item Tous les commits \textbf{\textsc{après}} \texttt{<commit-id>} ont été supprimés, L'historique de votre dépôt a été réécrit.
        \\(Si vous voulez laisser l'historique inchangé, allez à la slide~\ref{how-to-revert-pushed-commit-keep-history}).
    \end{enumerate}
    \end{small}
    \end{flushleft}
\end{frame}

\subsection{Revenir à un commit spécifique d'une branche (en gardant l'historique après ce dernier)}
\begin{frame}{[SPs] Revenir à un commit spécifique d'une branche \textsc{\emph{(en gardant l'historique après ce dernier)}}} \label{how-to-revert-pushed-commit-keep-history}
    \begin{flushleft}
    \begin{enumerate}
        \item Trouvez l'id du commit auquel vous voulez retourner en utilisant la commande: \\
        \quad \texttt{git log}

        \item Créez une nouvelle branche avec un commit initial pour celle-ci grâce à la commande: \\
        \quad \texttt{git branch <new-branch-name> <commit-id>}\\
        \quad\quad où \texttt{<new-branch-name>} est le nom d'une nouvelle branche,\\
        \quad\quad et \texttt{<commit-id>} est l'id du commit auquel on veut retourner.

        \item Publiez la nouvelle branche sur un dépôt distant en utilisant la commande:\\
        \quad \texttt{git push origin <new-branch-name>}
    \end{enumerate}
    \end{flushleft}
\end{frame}

\begin{frame}{[SPs] Revenir à un commit spécifique d'une branche \textsc{\emph{(en gardant l'historique après ce dernier)}}}
    \begin{flushleft}
    Cette technique peut être utile pour certaines situations:
    \begin{itemize}
        \item Travailler sur plusieures versions d'un même programme/projet dans l'objectif de les comparer et de juger lesquelles sont les meilleures,
        \item Retravailler une ancienne partie d'un projet en s'inspirant de ce qui a déjà été fait (Utile pour des projets où il est nécessaire et complexe de modifier certaines parties et où il est plus pertinant de revenir à une ancienne version),
        \item D'une manière générale, pour revenir sur une ancienne version du projet sans supprimer ce qui a été déjà réalisé.
    \end{itemize}
    \end{flushleft}
\end{frame}

\subsection{Mettre à jour un fork}
\begin{frame}{[SPs] Mettre à jour un fork}
    \begin{flushleft}
    \begin{small}
    \begin{enumerate}
        \item Ajoutez un remote en HTTPS/SSH grâce à la commande: \\
        \quad \footnotesize{\texttt{git remote add <remote> <remote-url>}} \\
        \quad \quad où \texttt{<remote>} $\Rightarrow$ \texttt{upstream} (ou autre $\neq$ \texttt{origin}),\\
        \quad \quad où \texttt{<remote-url>} est de la forme suivante:

        \begin{table}[H]
            \centering
            \begin{tabular}{|lr|}
                \hline
                \scriptsize{HTTPS:} & \tiny{\texttt{https://gitlab.com/louvainlinux/atelier-git.git}}\\
                \scriptsize{SSH:} & \tiny{\texttt{git@gitlab.com:louvainlinux/atelier-git.git}}\\
                \hline
            \end{tabular}
        \end{table}

        \item Récupérez les données liées à \texttt{<remote>}:\\
        \quad \footnotesize{\texttt{git fetch <remote-name>}}
        \item Merger une de vos branche \texttt{<branch>} avec une de celle de \texttt{<remote>} (nommée \texttt{<remote-branch>}):\\
        \quad \footnotesize{\texttt{git merge <remote>/<remote-branch> <branch>}}\\
        \quad Exemple: \footnotesize{\texttt{git merge upstream/main main}}\\
        \item (Réglez les conflits (si il y en a, ajoutez (\texttt{add}) vos modification et faites un \texttt{commit}).)
        \item Faites un \texttt{push} pour mettre à jour votre dépôt distant.
    \end{enumerate}
    \end{small}
    \end{flushleft}
\end{frame}

%-------------------------------------------------------------------%

\section{Informations et ressources}
\begin{frame}{Gitlab, Github, Bitbucket}
    \begin{center}
        \href{https://gitlab.com}{\includegraphics[width=0.45\textwidth]{../img/logo/gitlab.png}}\href{https://github.com}{\includegraphics[width=0.45\textwidth]{../img/logo/github.png}}\\
        \href{https://bitbucket.org}{\includegraphics[width=0.75\textwidth]{../img/logo/bitbucket.png}}
    \end{center}
    Pratiquement identiques (tous fonctionnent avec GitHub Desktop).
\end{frame}

\begin{frame}{Github Student Pack}
    Dépôts privés gratuits (tout comme sur Gitlab \& Bitbucket), et d'autres avantages pour les informaticiens : \url{https://education.github.com/pack}.

    Nécessite d'ajouter l'adresse \texttt{...@student.uclouvain.be} au compte
    GitHub.
\end{frame}

\begin{frame}{Interface en ligne de commande} \label{install-cli}
    Utilisée par beaucoup de gens, très puissante si vous êtes à l'aise avec
    un terminal.

    Installation:
    \begin{itemize}
        \item \textbf{Ubuntu} : \texttt{sudo apt-get install git}
        \item \textbf{OS X} : \url{https://sourceforge.net/projects/git-osx-installer/}
        \item \textbf{Windows} : \url{https://git-for-windows.github.io/} (déjà
            installé à l'UCL)
    \end{itemize}

    Documentation:
    \begin{itemize}
        \item \textbf{La référence: Git book}: \url{https://git-scm.com/book}:
            abordable, bien expliqué et très complet !
        \item \texttt{git help} \\
              \texttt{git <commande> --help}
    \end{itemize}
\end{frame}

\begin{frame}{Autres interfaces graphiques}
    \begin{itemize}
        \item \url{https://git-scm.com/docs/gitk} (Installé par défaut sur PC UCL)
        \item \url{https://www.gitkraken.com/}
%        \item \url{https://desktop.github.com/}
        \item D'autres: \url{https://git-scm.com/downloads/guis}
    \end{itemize}
\end{frame}

\begin{frame}[standout]
    Questions ?
\end{frame}

\end{document}

