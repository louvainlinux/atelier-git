# Flags
LATEX_FLAGS := -pdf -lualatex -cd -silent
TEXTIDOTE := textidote
TEXTIDOTE_JAVA := java -jar check_spelling/textidote.jar
TEXTIDOTE_FLAGS := --check fr --output html
TEXTIDOTE_DICT := --dict check_spelling/check_words_whitelist.txt

# Directories
THEORY = theory
EXERCISE = exercises
THEORY_TEX = src/$(THEORY)/$(THEORY).tex
EXERCISE_TEX = src/$(EXERCISE)/$(EXERCISE).tex
THEORY_CHECK = check_spelling/$(THEORY).html
EXERCISE_CHECK = check_spelling/$(EXERCISE).html
THEORY_PDF = build_latex/$(THEORY).pdf
EXERCISE_PDF = build_latex/$(EXERCISE).pdf

# ------------------------------------------------------------------

.PHONY: all $(THEORY) $(EXERCISE) open clean clean_aux

all: open

open: $(THEORY) $(EXERCISE)
	@xdg-open $(EXERCISE_PDF) 2>/dev/null 1>/dev/null &
	@xdg-open $(THEORY_PDF) 2>/dev/null 1>/dev/null &

$(THEORY): $(THEORY_PDF)
$(EXERCISE): $(EXERCISE_PDF)

check:
	$(TEXTIDOTE) $(TEXTIDOTE_FLAGS) $(TEXTIDOTE_DICT) $(THEORY_TEX) > $(THEORY_CHECK) &
	$(TEXTIDOTE) $(TEXTIDOTE_FLAGS) $(TEXTIDOTE_DICT) $(EXERCISE_TEX) > $(EXERCISE_CHECK)

check_jar: 
	$(TEXTIDOTE_JAVA) $(TEXTIDOTE_FLAGS) $(TEXTIDOTE_DICT) $(THEORY_TEX) > $(THEORY_CHECK) &
	$(TEXTIDOTE_JAVA) $(TEXTIDOTE_FLAGS) $(TEXTIDOTE_DICT) $(EXERCISE_TEX) > $(EXERCISE_CHECK)

clean:
	rm -r build_latex/*

clean-aux:
	rm $(shell find build_latex/* -not -type d -not -path '$(THEORY_PDF)' -not -path '$(EXERCISE_PDF)')

$(THEORY_PDF): $(THEORY_TEX)
	@echo -e "\e[1;7;32m[=]\e[27m Compiling $< to $@ ...\e[0m"
	latexmk $(LATEX_FLAGS) $(LATEX_OPT) -outdir=$(PWD)/$(@D) $< $(BASH_POSTPROCESSING)

$(EXERCISE_PDF): $(EXERCISE_TEX)
	@echo -e "\e[1;7;32m[=]\e[27m Compiling $< to $@ ...\e[0m"
	latexmk $(LATEX_FLAGS) $(LATEX_OPT) -outdir=$(PWD)/$(@D) $< $(BASH_POSTPROCESSING)


